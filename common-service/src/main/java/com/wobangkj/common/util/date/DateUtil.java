package com.wobangkj.common.util.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author dreamlu
 * @date 2019/06/22
 */
public class DateUtil {

	/**
	 * 获取几天前后日期
	 *
	 * @return
	 */
	public static String getTime(Integer day) {
		SimpleDateFormat df       = new SimpleDateFormat("yyyy-MM-dd");
		Date             date     = new Date();
		Calendar         calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, day);
		date = calendar.getTime();
		return df.format(date);
	}
}
